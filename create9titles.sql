DROP TABLE titles;
-- DROP TABLE
CREATE OR REPLACE VIEW titles AS
    SELECT thist.emp_no,tref.title,thist.from_date,thist.to_date
    FROM titles_history thist, title_ref tref
    WHERE thist.tit_no = tref.tit_no;
-- CREATE VIEW
