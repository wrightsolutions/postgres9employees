CREATE TABLE salaries_hire_year_eighties (
  CONSTRAINT salaries_hire_year_eighties_check
  CHECK ( hire_year < 1990 ),
  CONSTRAINT cs_salaries_hire_year_eighties_pkey PRIMARY KEY (emp_no,from_date)
) INHERITS (salaries_hire_year);

CREATE INDEX salaries_hire_year_eighties_emp_no_idx ON salaries_hire_year_eighties (emp_no);
CREATE INDEX salaries_hire_year_eighties_hire_year_idx ON salaries_hire_year_eighties (hire_year) WITH (fillfactor=90);
-- ALTER TABLE salaries_hire_year_eighties ADD CONSTRAINT cs_salaries_hire_year_eighties_pkey PRIMARY KEY (emp_no,from_date);
-- ALTER TABLE salaries_hire_year_eighties ADD CONSTRAINT
-- cs_salaries_hire_year_eighties_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;

-- Indexes:
--    "cs_salaries_hire_year_eighties_pkey" PRIMARY KEY, btree (emp_no, from_date)
--    "salaries_hire_year_eighties_emp_no_idx" btree (emp_no)
--    "salaries_hire_year_eighties_hire_year_idx" btree (hire_year) WITH (fillfactor=90)
-- Check constraints:
--    "salaries_hire_year_eighties_check" CHECK (hire_year < 1990)
-- Inherits: salaries_hire_year
