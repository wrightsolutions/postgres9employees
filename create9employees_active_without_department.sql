CREATE OR REPLACE VIEW employees_active_without_department AS
SELECT e.emp_no,
e.leave_date,
de.to_date AS dept_emp_to_date,
thist.from_date AS titles_history_from_date,
thist.to_date AS titles_history_to_date
FROM dept_emp de,
employees e,
titles_history thist
WHERE de.emp_no = e.emp_no
AND e.emp_no = thist.emp_no
AND de.to_date < '9998-01-01'
AND e.leave_date IS NULL
ORDER BY emp_no,
titles_history_from_date ASC
;
