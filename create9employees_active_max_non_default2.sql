CREATE OR REPLACE VIEW employees_active_max_non_default2 AS
SELECT e.emp_no,
e.leave_date,
max(de.to_date) AS dept_emp_to_date_max,
max(thist.to_date) AS titles_history_to_date_max
FROM dept_emp de,
employees e,
titles_history thist
WHERE de.emp_no = e.emp_no
AND e.emp_no = thist.emp_no
AND de.to_date < '9998-01-01'
AND thist.to_date < '9998-01-01'
AND e.leave_date IS NULL
GROUP BY e.emp_no,
e.leave_date
;
