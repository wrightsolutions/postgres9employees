CREATE OR REPLACE VIEW titles_history_max AS
  SELECT th.*
  FROM titles_history th,
  (SELECT emp_no,
  max(from_date) AS from_date_max
  FROM titles_history
  GROUP BY emp_no) AS tm
  WHERE th.emp_no = tm.emp_no
  AND th.from_date = tm.from_date_max
;
-- CREATE UNIQUE INDEX titles_history_emp_no_from_date_key ON titles_history (emp_no,from_date);
