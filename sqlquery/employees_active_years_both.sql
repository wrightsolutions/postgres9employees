SELECT e.emp_no,
e.leave_date,
de.to_date AS dept_emp_to_date,
EXTRACT(YEAR FROM (de.to_date)) AS dept_emp_to_date_year,
thist.from_date AS titles_history_from_date,
thist.to_date AS titles_history_to_date,
EXTRACT(YEAR FROM (thist.to_date)) AS titles_history_to_date_year
FROM dept_emp de,
employees e,
titles_history thist
WHERE de.emp_no = e.emp_no
AND e.emp_no = thist.emp_no
AND de.to_date < '9998-01-01'
AND e.leave_date IS NULL
ORDER BY emp_no,
titles_history_from_date ASC
;
-- Above query is a 'loose' look at the data and should be treated with caution
-- Are the joins strong enough for your particular query requirement?
--
-- Think of this as a template which you should tighten up depending on your
-- particular data cleanup exercise.
