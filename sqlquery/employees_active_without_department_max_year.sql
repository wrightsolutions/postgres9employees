SELECT e.emp_no,
e.leave_date,
max(de.to_date) AS max_dept_emp_to_date,
max(thist.to_date) AS max_thist_to_date
FROM dept_emp de,
employees e,
titles_history thist
WHERE de.emp_no = e.emp_no
AND e.emp_no = thist.emp_no
AND de.to_date < '9998-01-01'
AND e.leave_date IS NULL
GROUP BY e.emp_no,
e.leave_date
;
