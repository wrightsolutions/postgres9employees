SELECT e.emp_no,
e.last_name,
tm.from_date AS title_from_date,
tm.to_date AS title_to_date,
dm.from_date AS dept_emp_from_date,
dm.to_date AS dept_emp_to_date
FROM employees e,
titles_history_year_month_max tm,
dept_emp_year_month_max dm,
CASE WHEN tm.to_date < dm.to_date THEN -1
WHEN tm.to_date > dm.to_date THEN 1
ELSE 0 END AS compared_val_to
WHERE e.emp_no = tm.emp_no
AND e.emp_no = dm.emp_no
AND tm.emp_no = dm.emp_no
ORDER BY emp_no
WHERE emp_no BETWEEN 10700 AND 10720
;
