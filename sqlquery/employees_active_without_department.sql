SELECT e.emp_no,
e.leave_date,
de.to_date AS de_to_date,
thist.from_date AS thist_from_date,
thist.to_date AS thist_to_date
FROM dept_emp de,
employees e,
titles_history thist
WHERE de.emp_no = e.emp_no
AND e.emp_no = thist.emp_no
AND de.to_date < '9998-01-01'
AND e.leave_date IS NULL
ORDER BY emp_no,
thist_from_date ASC
;
