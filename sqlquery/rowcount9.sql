select count(*) as departments_rowcount from departments;
select count(*) as dept_manager_rowcount from dept_manager;
select count(*) as employees_rowcount from employees;
select count(*) as dept_emp_rowcount from dept_emp;
select count(*) as titles_history_rowcount from titles_history;
select count(*) as salaries_rowcount from salaries;
select count(*) as view_employees_active_rowcount from employees_active;
select count(*) as view_employees_leaver_rowcount from employees_leaver;
