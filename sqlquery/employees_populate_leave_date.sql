UPDATE employees SET leave_date = t.max_to_date FROM (
select titles.emp_no,max(to_date) as max_to_date from titles group by titles.emp_no
) as t
WHERE employees.emp_no = t.emp_no and t.max_to_date < '9998-01-01';
