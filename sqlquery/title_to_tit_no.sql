CREATE TABLE titles_history2 AS TABLE titles;
ALTER TABLE titles_history2 ADD COLUMN tit_no int4;
UPDATE titles_history2 SET tit_no=1 WHERE title = 'Engineer';
-- UPDATE 115003
UPDATE titles_history2 SET tit_no=2 WHERE title = 'Senior Engineer';
-- UPDATE 97750
UPDATE titles_history2 SET tit_no=3 WHERE title = 'Assistant Engineer';
-- UPDATE 15128
UPDATE titles_history2 SET tit_no=31 WHERE title = 'Technique Leader';
-- UPDATE 15159
UPDATE titles_history2 SET tit_no=51 WHERE title = 'Staff';
-- UPDATE 107391
UPDATE titles_history2 SET tit_no=52 WHERE title = 'Senior Staff';
-- UPDATE 92853
UPDATE titles_history2 SET tit_no=100 WHERE title = 'Manager';
-- UPDATE 24
CREATE TABLE titles_history AS SELECT emp_no,tit_no,from_date,to_date FROM titles_history2 ORDER BY emp_no,from_date;
ALTER TABLE titles_history ALTER COLUMN tit_no SET NOT NULL;
-- DROP TABLE titles_history2;
