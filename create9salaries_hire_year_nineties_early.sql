CREATE TABLE salaries_hire_year_nineties_early (
  CONSTRAINT salaries_hire_year_nineties_early_check
  CHECK ( hire_year > 1989 and hire_year < 1995 ),
  CONSTRAINT cs_salaries_hire_year_nineties_early_pkey PRIMARY KEY (emp_no,from_date)
) INHERITS (salaries_hire_year);

CREATE INDEX salaries_hire_year_nineties_early_emp_no_idx ON salaries_hire_year_nineties_early (emp_no);
CREATE INDEX salaries_hire_year_nineties_early_hire_year_idx ON salaries_hire_year_nineties_early (hire_year) WITH (fillfactor=90);
-- ALTER TABLE salaries_hire_year_nineties_early ADD CONSTRAINT cs_salaries_hire_year_nineties_early_pkey PRIMARY KEY (emp_no,from_date);
-- ALTER TABLE salaries_hire_year_nineties_early ADD CONSTRAINT
-- cs_salaries_hire_year_nineties_early_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;

