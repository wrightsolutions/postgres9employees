employees=# SELECT count(*),EXTRACT(YEAR FROM hire_date) AS hire_year FROM employees GROUP BY hire_year ORDER BY hire_year;
 count | hire_year 
-------+-----------
 35316 |      1985
 36150 |      1986
 33501 |      1987
 31436 |      1988
 28394 |      1989
 25610 |      1990
 22568 |      1991
 20402 |      1992
 17772 |      1993
 14835 |      1994
 12115 |      1995
  9574 |      1996
  6669 |      1997
  4155 |      1998
  1514 |      1999
    13 |      2000
(16 rows)

employees=# SELECT count(*),EXTRACT(YEAR FROM hire_date) AS hire_year FROM employees WHERE leave_date IS NOT NULL GROUP BY hire_year ORDER BY hire_year;
 count | hire_year 
-------+-----------
  7025 |      1985
  7310 |      1986
  6817 |      1987
  6293 |      1988
  5573 |      1989
  5173 |      1990
  4451 |      1991
  4106 |      1992
  3395 |      1993
  2948 |      1994
  2372 |      1995
  1913 |      1996
  1377 |      1997
   833 |      1998
   310 |      1999
     4 |      2000
(16 rows)

employees=# SELECT count(*),EXTRACT(YEAR FROM hire_date) AS hire_year FROM employees WHERE leave_date IS NULL GROUP BY hire_year ORDER BY hire_year;
 count | hire_year 
-------+-----------
 28291 |      1985
 28840 |      1986
 26684 |      1987
 25143 |      1988
 22821 |      1989
 20437 |      1990
 18117 |      1991
 16296 |      1992
 14377 |      1993
 11887 |      1994
  9743 |      1995
  7661 |      1996
  5292 |      1997
  3322 |      1998
  1204 |      1999
     9 |      2000
(16 rows)
