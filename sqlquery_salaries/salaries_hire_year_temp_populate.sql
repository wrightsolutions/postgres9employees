CREATE TABLE salaries_hire_year_temp (LIKE salaries INCLUDING DEFAULTS);
-- CREATE TABLE
INSERT INTO salaries_hire_year_temp SELECT * FROM salaries;
-- INSERT 0 2844047
ALTER TABLE salaries_hire_year_temp ADD COLUMN hire_year smallint;
-- ALTER TABLE
UPDATE salaries_hire_year_temp sal 
SET hire_year = 
(SELECT EXTRACT(YEAR FROM emp.hire_date) 
FROM employees emp 
WHERE emp.emp_no = sal.emp_no)
;
-- UPDATE 2844047
ALTER TABLE salaries_hire_year_temp ALTER COLUMN hire_year SET NOT NULL;
-- ALTER TABLE
