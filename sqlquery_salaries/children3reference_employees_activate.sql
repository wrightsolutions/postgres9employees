ALTER TABLE salaries_hire_year_eighties ADD CONSTRAINT
cs_salaries_hire_year_eighties_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
--
ALTER TABLE salaries_hire_year_nineties_early ADD CONSTRAINT
cs_salaries_hire_year_nineties_early_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
--
ALTER TABLE salaries_hire_year_other ADD CONSTRAINT
cs_salaries_hire_year_other_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
--
