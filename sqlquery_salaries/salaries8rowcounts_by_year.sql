SELECT count(*),
EXTRACT(YEAR FROM hire_date) AS hire_year
FROM employees
GROUP BY hire_year
ORDER BY hire_year
;

SELECT count(*),
EXTRACT(YEAR FROM hire_date) AS hire_year
FROM employees
WHERE leave_date IS NOT NULL
GROUP BY hire_year
ORDER BY hire_year
;

SELECT count(*),
EXTRACT(YEAR FROM hire_date) AS hire_year
FROM employees
WHERE leave_date IS NULL
GROUP BY hire_year
ORDER BY hire_year
;
