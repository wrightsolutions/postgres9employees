SELECT count(emp.*)
FROM employees_active emp
WHERE NOT EXISTS
(SELECT emp_no
FROM employees_active_without_department ew
WHERE ew.emp_no = emp.emp_no
)
;



SELECT emp.*
FROM employees_active emp
WHERE NOT EXISTS
(SELECT emp_no
FROM employees_active_without_department ew
WHERE ew.emp_no = emp.emp_no
)
AND emp.emp_no
BETWEEN 10700 and 10720
;

