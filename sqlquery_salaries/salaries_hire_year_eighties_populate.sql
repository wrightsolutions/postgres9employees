INSERT INTO salaries_hire_year_eighties
SELECT st.emp_no,st.salary,
st.from_date,
st.to_date,
st.hire_year
FROM salaries_hire_year_temp st
WHERE st.hire_year < 1990
;

SELECT hire_year,
count(*)
FROM salaries_hire_year_temp
WHERE hire_year < 1990
GROUP BY hire_year
ORDER BY hire_year
;
