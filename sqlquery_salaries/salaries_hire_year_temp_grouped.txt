employees=# SELECT hire_year,count(*) FROM salaries_hire_year_temp GROUP BY hire_year ORDER BY hire_year;
 hire_year | count  
-----------+--------
      1985 | 443143
      1986 | 428804
      1987 | 375583
      1988 | 330656
      1989 | 281055
      1990 | 235482
      1991 | 193137
      1992 | 160107
      1993 | 128277
      1994 |  96450
      1995 |  71003
      1996 |  49520
      1997 |  29961
      1998 |  15992
      1999 |   4841
      2000 |     36
(16 rows)
