INSERT INTO salaries_hire_year_nineties_early
SELECT st.emp_no,st.salary,
st.from_date,
st.to_date,
st.hire_year
FROM salaries_hire_year_temp st
WHERE st.hire_year > 1989
AND st.hire_year < 1995
;
