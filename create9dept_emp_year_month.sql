CREATE OR REPLACE VIEW dept_emp_year_month AS
SELECT de.emp_no,
de.dept_no,
de.from_date,
EXTRACT(YEAR FROM (de.from_date)) AS from_date_year,
EXTRACT(MONTH FROM (de.from_date)) AS from_date_month,
de.to_date,
EXTRACT(YEAR FROM (de.to_date)) AS to_date_year,
EXTRACT(MONTH FROM (de.to_date)) AS to_date_month,
e.hire_date,
e.leave_date,
EXTRACT(YEAR FROM (e.leave_date)) AS leave_date_year
FROM dept_emp de,
employees e
WHERE de.emp_no = e.emp_no
ORDER BY de.from_date ASC
;
-- e.hire_date AS emp_hire_date,
-- e.leave_date AS emp_leave_date,
