CREATE TABLE salaries_hire_year_other (
  CONSTRAINT salaries_hire_year_other_check
  CHECK ( hire_year > 1994 ),
  CONSTRAINT cs_salaries_hire_year_other_pkey PRIMARY KEY (emp_no,from_date)
) INHERITS (salaries_hire_year);

CREATE INDEX salaries_hire_year_other_emp_no_idx ON salaries_hire_year_other (emp_no);
CREATE INDEX salaries_hire_year_other_hire_year_idx ON salaries_hire_year_other (hire_year) WITH (fillfactor=60);
-- ALTER TABLE salaries_hire_year_other ADD CONSTRAINT cs_salaries_hire_year_other_pkey PRIMARY KEY (emp_no,from_date);
-- ALTER TABLE salaries_hire_year_other ADD CONSTRAINT
-- cs_salaries_hire_year_other_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;

