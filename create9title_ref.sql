CREATE TABLE title_ref (
  tit_no int4 NOT NULL,
  title varchar(50) NOT NULL,
  CONSTRAINT cs_title_ref_pkey PRIMARY KEY (tit_no)
)
WITH (fillfactor=70);
CREATE UNIQUE INDEX title_ref_title_idx ON title_ref (title) WITH (fillfactor = 70);
-- ALTER TABLE title_ref ADD CONSTRAINT cs_title_ref_title_key UNIQUE (title)
-- ALTER TABLE title_ref ADD CONSTRAINT cs_title_ref_pkey PRIMARY KEY (tit_no);
