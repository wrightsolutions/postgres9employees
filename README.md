# Test database 'employees'

This is a sample database for testing purposes.
  
The original data was created by Fusheng Wang and Carlo Zaniolo at 
Siemens Corporate Research.  
The data is in XML format.
http://www.cs.aau.dk/TimeCenter/software.htm
http://www.cs.aau.dk/TimeCenter/Data/employeeTemporalDataSet.zip

Giuseppe Maxia made the relational schema and Patrick Crews exported
the data in relational format.

This Postgresql port is a derivative under same licensing

This work is licensed under the 
Creative Commons Attribution-Share Alike 3.0 Unported License.  
To view a copy of this license, visit 
http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to 
Creative Commons, 171 Second Street, Suite 300, San Francisco, 
California, 94105, USA.


## Postgresql port

See the files in doc/

[Postgresql port](doc/README.md)

This Postgresql port is a derivative under same licensing

