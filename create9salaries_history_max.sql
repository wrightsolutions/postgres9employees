CREATE OR REPLACE VIEW salaries_history_max AS
  SELECT s.*
  FROM salaries_hire_year s,
  (SELECT emp_no,
  max(from_date) AS from_date_max
  FROM salaries_hire_year
  GROUP BY emp_no) AS sm
  WHERE s.emp_no = sm.emp_no
  AND s.from_date = sm.from_date_max
;
-- ALTER TABLE salaries_hire_year_eighties ADD CONSTRAINT cs_salaries_hire_year_eighties_pkey PRIMARY KEY (emp_no,from_date);
