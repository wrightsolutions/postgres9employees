CREATE TABLE dept_emp (
  emp_no int4 NOT NULL,
  dept_no char(4) NOT NULL,
  from_date date NOT NULL,
  to_date date NOT NULL,
  CONSTRAINT cs_dept_emp_pkey PRIMARY KEY (emp_no,dept_no)
);
CREATE INDEX dept_emp_dept_no_idx ON dept_emp (dept_no);
CREATE INDEX dept_emp_emp_no_idx ON dept_emp (emp_no);
CREATE INDEX dept_emp_from_date_idx ON dept_emp (from_date);
CREATE UNIQUE INDEX dept_emp_emp_no_from_date_key ON titles_history (emp_no,from_date);
-- ALTER TABLE dept_emp ADD CONSTRAINT cs_dept_emp_pkey PRIMARY KEY (emp_no,dept_no);
-- ALTER TABLE dept_emp ADD CONSTRAINT
-- cs_dept_emp_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
-- ALTER TABLE dept_emp ADD CONSTRAINT
-- cs_dept_emp_dept_no_fkey1 FOREIGN KEY (dept_no) REFERENCES departments (dept_no) ON DELETE CASCADE;
-- ALTER TABLE dept_emp ADD CONSTRAINT cs_dept_emp_emp_no_from_date_key UNIQUE (emp_no,from_date);
