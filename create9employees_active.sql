CREATE OR REPLACE VIEW employees_active AS
    SELECT *
    FROM employees
    WHERE leave_date IS NULL;
