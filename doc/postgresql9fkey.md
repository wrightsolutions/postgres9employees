# Foreign keys Postgresql

## Preventing orphan records for removed employees (1), (3), (5), (6) in original list

+ dept_emp: "cs_dept_emp_emp_no_fkey1" FOREIGN KEY (emp_no) REFERENCES employees(emp_no) ON DELETE CASCADE
+ dept_manager: "cs_dept_manager_emp_no_fkey1" FOREIGN KEY (emp_no) REFERENCES employees(emp_no) ON DELETE CASCADE
+ salaries_hire_year_eighties: "cs_salaries_hire_year_eighties_emp_no_fkey1" FOREIGN KEY (emp_no) REFERENCES employees(emp_no) ON DELETE CASCADE
+ salaries_hire_year_nineties_early: "cs_salaries_hire_year_nineties_early_emp_no_fkey1" FOREIGN KEY (emp_no) REFERENCES employees(emp_no) ON DELETE CASCADE
+ salaries_hire_year_other: "cs_salaries_hire_year_other_emp_no_fkey1" FOREIGN KEY (emp_no) REFERENCES employees(emp_no) ON DELETE CASCADE
+ titles_history: "cs_titles_history_emp_no_fkey1" FOREIGN KEY (emp_no) REFERENCES employees(emp_no) ON DELETE CASCADE


## Preventing orphan records for removed departments (2), (4) in original list

+ dept_emp: "cs_dept_emp_dept_no_fkey1" FOREIGN KEY (dept_no) REFERENCES departments(dept_no) ON DELETE CASCADE
+ dept_manager: "cs_dept_manager_dept_no_fkey1" FOREIGN KEY (dept_no) REFERENCES departments(dept_no) ON DELETE CASCADE


## Preventing orphan records for removed titles

+ titles_history: "cs_titles_history_tit_no_fkey1" FOREIGN KEY (tit_no) REFERENCES title_ref(tit_no) ON DELETE CASCADE


## Original foreign keys reference employees and departments from the other 4 tables:

1. cc_dept_emp.mysql:  CONSTRAINT `dept_emp_ibfk_1` FOREIGN KEY (`emp_no`) REFERENCES `employees` (`emp_no`) ON DELETE CASCADE,
2. cc_dept_emp.mysql:  CONSTRAINT `dept_emp_ibfk_2` FOREIGN KEY (`dept_no`) REFERENCES `departments` (`dept_no`) ON DELETE CASCADE
3. cc_dept_manager.mysql:  CONSTRAINT `dept_manager_ibfk_1` FOREIGN KEY (`emp_no`) REFERENCES `employees` (`emp_no`) ON DELETE CASCADE,
4. cc_dept_manager.mysql:  CONSTRAINT `dept_manager_ibfk_2` FOREIGN KEY (`dept_no`) REFERENCES `departments` (`dept_no`) ON DELETE CASCADE
5. cc_salaries.mysql:  CONSTRAINT `salaries_ibfk_1` FOREIGN KEY (`emp_no`) REFERENCES `employees` (`emp_no`) ON DELETE CASCADE
6. cc_titles.mysql:  CONSTRAINT `titles_ibfk_1` FOREIGN KEY (`emp_no`) REFERENCES `employees` (`emp_no`) ON DELETE CASCADE

