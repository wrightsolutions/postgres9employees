# Custom views to support data analysis - salaries data

## salaries_history_max

Most recent salary for each employee



## salaries_history_year_month

View that adds year and month fields for each title assignment

Extra fields are named:  

* from_date_year
* from_date_month
* to_date_year
* to_date_month
* leave_date_year


## salaries_history_year_month_max

Most recent salary for each employee filtering
of view salaries_history_year_month

DEPENDS on other view so this view must be dropped first if
you intend to rework the views on which it depends  
DEPENDS on:  

* salaries_history_year_month


## salaries data generally and joining on other fields

As salaries data (salaries_hire_year table children) have a unique index / key on (emp_no, from_date),  
it is possible to use salaries_hire_year from_date in that context as a join field.

How reliable is it to use such a join field?  
Argue this based on your own data analysis as you become more familiar with the data.

