# Custom views to support data analysis - employees data

## employees_active

Filter on employees 
WHERE leave_date IS NULL

This is one interpretation of an 'active' employee
based on title assignment which was used to populate leave_date


## employees_leaver

Filter on employees
WHERE leave_date IS NOT NULL

See note above regarding title assignment and leave_date


## employees_active_without_department

Helper for analysis of titles assignment versus department assignment
for employees that have null leave_date

Filters using to_date < '9998-01-01'


## employees_active_max_non_default2

Helper for analysis of titles assignment versus department assignment
for employees that have null leave_date

Employee number (emp_no) is the joiner for this 3 table join

Filters on to_dates as follows:  

+ dept_emp.to_date < '9998-01-01'
+ titles_history.to_date < '9998-01-01'

