# Test database 'employees' - Postgresql port

This is a sample database for testing purposes.
  

## titles data

Looking at the schema, it looks like the titles data is
setup to support audit.

This would explain why the titles data has dated entries for titles,
rather than just a current title for each employee only.

We do not want to lose this historical data, however having the
word 'Engineer' repeated several thousand times could be replaced
by database functionality, rather than being repetitive entries.


## table title_ref

This new small reference table contains the following entries:  

* Engineer
* Senior Engineer
* Assistant Engineer
* Technique Leader
* Staff
* Senior Staff
* Manager

## table titles_history

This new titles_history table supports the same sort of data
as the original title table, however it references the new
title_ref table rather than storing title strings within the data

New field title number (tit_no) is the link to title_ref

A new composite UNIQUE index has been added on (emp_no,from_date)
making another index available to queries that include max(from_date)

## titles - the VIEW

titles is now a VIEW with a from clause that pulls from titles_history and title_ref
to form an analogue of the original 'titles' table 


## Credits

Credits for data/schema/export (see root README)
but in particular

Original: Fusheng Wang, Carlo Zaniolo, Giuseppe Maxia, Patrick Crews

This Postgresql porting: Gary Wright
