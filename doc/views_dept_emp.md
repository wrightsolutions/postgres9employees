# Custom views to support data analysis - dept_emp data

## dept_emp_max

Most recent department assignment for each employee

Subquery uses max(from_date)

Ordered on emp_no


## dept_emp_year_month

Helper to compare department assignment and leave date
side by side

Joins dept_emp and employee to get to leave date

Useful also if you want to do analysis of department assigments
by year and month


## dept_emp_year_month_max

Most recent department assignment for each employee filtering
of view dept_emp_year_month

DEPENDS on other view so this view must be dropped first if
you intend to rework the views on which it depends  
DEPENDS on:  

* dept_emp_year_month

