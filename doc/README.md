# Test database 'employees' - Postgresql port

This is a sample database for testing purposes.
  

## Project brief

This port has been coupled with a theoretical data migration project

The brief here is that the existing data is from a historical database of
personnel and payroll. That system has now been replaced.

However some reporting functions are still active.

In particular for audit, the data should be retained.

You should aim to bring the data into Postgresql making use
of any relevant Postgresql features

Changes should affect current reporting jobs as little as possible,
but remember that the system itself is not fully active for updates,
having been replaced by an outsourced personnel and payroll function.


## Implementation details

Based on the brief my approach is:  

+ Replace tables with views if necessary
+ Adding columns is fine but try not to retask or change existing columns or data entries as much as possible

Data areas with additional notes:  

+ [titles](titles.md)
+ [salaries](salaries.md)

Table 'employees' now has two extra columns at the end so as not to affect column order:  

+ leave_date
+ tit_no

leave_date has been populated based on analysis of titles history.  
( The view 'titles_history_year_month_max' defined later will help you see the row that was used [if appropriate] )

Although title number (tit_no) has been added to 'employees', it has not yet been populated.

A good exercise in analysis, possibly using the predefined views would be to populate title number in employees
with the most recent title number for each employee.


## Views to support data analysis

These view are not an essential part of the port however you might find them convenient for data analysis

+ [views for employees and related](views_employees.md)
+ [views for department assignment and related](views_dept_emp.md)
+ [views for salaries and related](views_salaries.md)
+ [views for titles assignment and related](views_titles.md)


## Data links 

The data is larger than would normally be stored in source control

Postgresql 9.2 database dump suitable for single pass creation of the data can be accessed at links given next.  
(The second of the links shown below is a 33 meg download)

+ [employees9 pg_dump manifest](http://publictarrup.fabe5c00.cdn.memsites.com/employees9allV1.list)
+ [employees9 pg_dump](http://publictarrup.fabe5c00.cdn.memsites.com/employees9allV1.pg_dump)


## Using the pg_dump file

pg_restore -d employees employees9allV1.pg_dump  
(If you instead try pushing the file through psql then you will likely see ERROR:  invalid byte sequence)


## Loading table by table 

Having foreign keys in place before the data load
is not an issue provided that employees is loaded before dept_manager


## Referential integrity - Foreign keys

[Postgresql foreign keys](postgresql9fkey.md)


## Credits

Credits for data/schema/export (see root README)
but in particular

Original: Fusheng Wang, Carlo Zaniolo, Giuseppe Maxia, Patrick Crews

This Postgresql porting: Gary Wright

