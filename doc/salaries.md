# Test database 'employees' - Postgresql port

This is a sample database for testing purposes.
  

## salaries data

Looking at the schema, it looks like the salaries data is
setup to support audit.

This would explain why the salaries data has dated entries,
rather than just a current salary for each employee only.

We do not want to lose this historical data.


## parent table 'salaries_hire_year'

A template / layout from which the child tables inherit

The data itself will be split across the following 3 child tables:  

+ salaries_hire_year_eighties
+ salaries_hire_year_nineties_early
+ create9salaries_hire_year_other

The split is on hire_year which has been populated by
extracting the year from hire_date of employee


## salaries_hire_year_eighties (child table)

CHECK ( hire_year < 1990 )


## salaries_hire_year_nineties_early (child table)

CHECK ( hire_year > 1989 and hire_year < 1995 )


## salaries_hire_year_other (child table)

CHECK ( hire_year > 1994 )


## salaries - the VIEW

salaries is now a VIEW with a from clause that pulls from salaries_hire_year
to form an analogue of the original 'salaries' table 

Includes an ORDER BY emp_no, from_date, to_date



## Credits

Credits for data/schema/export (see root README)
but in particular

Original: Fusheng Wang, Carlo Zaniolo, Giuseppe Maxia, Patrick Crews

This Postgresql porting: Gary Wright
