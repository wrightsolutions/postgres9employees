CREATE TABLE salaries_hire_year (
  emp_no int4 NOT NULL,
  salary int4 NOT NULL,
  from_date date NOT NULL,
  to_date date NOT NULL,
  hire_year smallint NOT NULL
  -- CONSTRAINT cs_salaries_hire_year_pkey PRIMARY KEY (emp_no,from_date)
);
-- CREATE INDEX salaries_hire_year_emp_no_idx ON salaries_hire_year (emp_no);
-- CREATE INDEX salaries_hire_year_hire_year_idx ON salaries_hire_year (hire_year);
-- ALTER TABLE salaries_hire_year ADD CONSTRAINT cs_salaries_hire_year_pkey PRIMARY KEY (emp_no,from_date);
-- ALTER TABLE salaries_hire_year ADD CONSTRAINT
-- cs_salaries_hire_year_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
