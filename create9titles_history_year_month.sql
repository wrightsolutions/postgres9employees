CREATE OR REPLACE VIEW titles_history_year_month AS
SELECT thist.emp_no,
thist.tit_no,
tref.title,
thist.from_date,
EXTRACT(YEAR FROM (thist.from_date)) AS from_date_year,
EXTRACT(MONTH FROM (thist.from_date)) AS from_date_month,
thist.to_date,
EXTRACT(YEAR FROM (thist.to_date)) AS to_date_year,
EXTRACT(MONTH FROM (thist.to_date)) AS to_date_month,
e.hire_date,
e.leave_date,
EXTRACT(YEAR FROM (e.leave_date)) AS leave_date_year
FROM titles_history thist,
employees e,
title_ref tref
WHERE thist.emp_no = e.emp_no
AND thist.tit_no = tref.tit_no
ORDER BY thist.from_date ASC
;
-- e.hire_date AS emp_hire_date,
-- e.leave_date AS emp_leave_date,
