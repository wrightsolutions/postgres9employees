CREATE OR REPLACE VIEW salaries AS
SELECT emp_no,
salary,
from_date,
to_date
FROM salaries_hire_year 
ORDER BY emp_no,
from_date,
to_date
;
