# Benchmarking

In this directory we have some very simple benchmarks.


## Benchmarking salaries data

Our purpose is to demonstrate that the inheritance structure for salaries
is not worse in every case

It is possible for an existing [unchanged] report to run slightly slower under the new setup.

We show in the following file one strategy for making better use of the new design:  
[benchmark 9 salaries example](benchmark9salaries_order_by20concurrent_inheritance.txt)

As the reports are reworked, the child tables can be accessed directly if appropriate.

However generally, if there is the possibility of needing any sort of table scan, then
the report designer will want to include hire_year predicate whenever it is logical.

