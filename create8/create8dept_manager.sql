CREATE TABLE dept_manager (
  dept_no char(4) NOT NULL,
  emp_no int4 NOT NULL,
  from_date date NOT NULL,
  to_date date NOT NULL,
  CONSTRAINT cs_dept_manager_pkey PRIMARY KEY (emp_no,dept_no)
);
CREATE INDEX dept_manager_dept_no_idx ON dept_manager (dept_no);
CREATE INDEX dept_manager_emp_no_idx ON dept_manager (emp_no);

-- ALTER TABLE dept_manager ADD CONSTRAINT cs_dept_manager_pkey PRIMARY KEY (emp_no,dept_no)
-- ALTER TABLE dept_manager ADD CONSTRAINT
-- cs_dept_manager_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
-- ALTER TABLE dept_manager ADD CONSTRAINT
-- cs_dept_manager_dept_no_fkey1 FOREIGN KEY (dept_no) REFERENCES departments (dept_no) ON DELETE CASCADE;

