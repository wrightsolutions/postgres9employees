CREATE TABLE salaries (
  emp_no int4 NOT NULL,
  salary int4 NOT NULL,
  from_date date NOT NULL,
  to_date date NOT NULL,
  CONSTRAINT cs_salaries_pkey PRIMARY KEY (emp_no,from_date)
);
CREATE INDEX salaries_emp_no_idx ON salaries (emp_no);
-- ALTER TABLE salaries ADD CONSTRAINT cs_salaries_pkey PRIMARY KEY (emp_no,from_date);
-- ALTER TABLE salaries ADD CONSTRAINT
-- cs_salaries_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
