CREATE TABLE titles (
  emp_no int4 NOT NULL,
  title varchar(50) NOT NULL,
  from_date date NOT NULL,
  to_date date DEFAULT NULL,
  CONSTRAINT cs_titles_pkey PRIMARY KEY (emp_no,title,from_date)
);
CREATE INDEX titles_emp_no_idx ON titles (emp_no);
-- ALTER TABLE titles ADD CONSTRAINT cs_titles_pkey PRIMARY KEY (emp_no,title,from_date);
-- ALTER TABLE titles ADD CONSTRAINT
-- cs_titles_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
