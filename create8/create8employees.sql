CREATE TYPE gender2enum AS ENUM ('M','F');
CREATE TABLE employees (
  emp_no int4 NOT NULL,
  birth_date date NOT NULL,
  first_name varchar(14) NOT NULL,
  last_name varchar(16) NOT NULL,
  gender gender2enum NOT NULL,
  hire_date date NOT NULL,
  CONSTRAINT cs_employees_pkey PRIMARY KEY (emp_no)
);
-- ALTER TABLE dept_manager ADD CONSTRAINT cs_employees_pkey PRIMARY KEY (emp_no)
