ALTER TABLE dept_emp ADD CONSTRAINT
cs_dept_emp_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
ALTER TABLE dept_emp ADD CONSTRAINT
cs_dept_emp_dept_no_fkey1 FOREIGN KEY (dept_no) REFERENCES departments (dept_no) ON DELETE CASCADE;
ALTER TABLE dept_manager ADD CONSTRAINT
cs_dept_manager_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
ALTER TABLE dept_manager ADD CONSTRAINT
cs_dept_manager_dept_no_fkey1 FOREIGN KEY (dept_no) REFERENCES departments (dept_no) ON DELETE CASCADE;
ALTER TABLE salaries ADD CONSTRAINT
cs_salaries_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
ALTER TABLE titles ADD CONSTRAINT
cs_titles_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
