CREATE TABLE departments (
  dept_no char(4) NOT NULL,
  dept_name varchar(40) NOT NULL,
  CONSTRAINT cs_departments_pkey PRIMARY KEY (dept_no),
  CONSTRAINT cs_departments_dept_name_key UNIQUE (dept_name)
);
-- ALTER TABLE departments ADD CONSTRAINT cs_departments_pkey PRIMARY KEY (dept_no);
-- ALTER TABLE departments ADD CONSTRAINT cs_departments_dept_name_key UNIQUE (dept_name);
