CREATE OR REPLACE VIEW dept_emp_year_month_max AS
  SELECT dey.*
  FROM dept_emp_year_month dey,
  (SELECT emp_no,
  max(from_date) AS from_date_max
  FROM dept_emp
  GROUP BY emp_no) AS dm
  WHERE dey.emp_no = dm.emp_no
  AND dey.from_date = dm.from_date_max
  ORDER BY emp_no
;
-- CREATE UNIQUE INDEX titles_history_emp_no_from_date_key ON titles_history (emp_no,from_date);
