CREATE TABLE titles_history (
  emp_no int4 NOT NULL,
  tit_no int4 NOT NULL,
  from_date date NOT NULL,
  to_date date DEFAULT NULL,
  CONSTRAINT cs_titles_history_pkey PRIMARY KEY (emp_no,tit_no,from_date)
);
CREATE INDEX titles_history_emp_no_idx ON titles_history (emp_no);
CREATE INDEX titles_history_tit_no_idx ON titles_history (tit_no);
CREATE UNIQUE INDEX titles_history_emp_no_from_date_key ON titles_history (emp_no,from_date);
-- ALTER TABLE titles_history ADD CONSTRAINT cs_titles_history_emp_no_from_date_key UNIQUE (emp_no,from_date);
-- ALTER TABLE titles_history ADD CONSTRAINT cs_titles_history_pkey PRIMARY KEY (emp_no,tit_no,from_date);
-- ALTER TABLE titles_history ADD CONSTRAINT
-- cs_titles_history_emp_no_fkey1 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE;
-- ALTER TABLE titles_history ADD CONSTRAINT
-- cs_titles_history_tit_no_fkey1 FOREIGN KEY (tit_no) REFERENCES title_ref (tit_no) ON DELETE CASCADE;
