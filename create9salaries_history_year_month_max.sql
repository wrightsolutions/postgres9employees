CREATE OR REPLACE VIEW salaries_history_year_month_max AS
  SELECT sal.*
  FROM salaries_history_year_month sal,
  (SELECT emp_no,
  max(from_date) AS from_date_max
  FROM salaries_history_year_month
  GROUP BY emp_no) AS sm
  WHERE sal.emp_no = sm.emp_no
  AND sal.from_date = sm.from_date_max
  ORDER BY sal.emp_no,
  sal.from_date DESC
;
-- ALTER TABLE salaries_hire_year_eighties ADD CONSTRAINT cs_salaries_hire_year_other_pkey PRIMARY KEY (emp_no,from_date);
