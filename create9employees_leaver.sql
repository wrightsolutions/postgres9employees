CREATE OR REPLACE VIEW employees_leaver AS
    SELECT *
    FROM employees
    WHERE leave_date IS NOT NULL;
