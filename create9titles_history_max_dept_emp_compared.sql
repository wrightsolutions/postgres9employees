CREATE OR REPLACE VIEW titles_history_max_dept_emp_compared AS
SELECT e.emp_no,
e.last_name,
tm.from_date AS title_from_date,
tm.to_date AS title_to_date,
dm.from_date AS dept_emp_from_date,
dm.to_date AS dept_emp_to_date,
CASE WHEN tm.from_date < dm.from_date THEN -1
WHEN tm.from_date > dm.from_date THEN 1
ELSE 0 END AS compared_val_from,
CASE WHEN tm.to_date < dm.to_date THEN -1
WHEN tm.to_date > dm.to_date THEN 1
ELSE 0 END AS compared_val_to
FROM employees e,
titles_history_year_month_max tm,
dept_emp_year_month_max dm
WHERE e.emp_no = tm.emp_no
AND e.emp_no = dm.emp_no
AND tm.emp_no = dm.emp_no
ORDER BY
compared_val_from ASC,
compared_val_to ASC,
e.emp_no ASC
;
-- CREATE UNIQUE INDEX titles_history_emp_no_from_date_key ON titles_history (emp_no,from_date);
