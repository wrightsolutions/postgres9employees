CREATE OR REPLACE VIEW salaries_history_year_month AS
SELECT sal.emp_no,
sal.from_date,
EXTRACT(YEAR FROM (sal.from_date)) AS from_date_year,
EXTRACT(MONTH FROM (sal.from_date)) AS from_date_month,
sal.to_date,
EXTRACT(YEAR FROM (sal.to_date)) AS to_date_year,
EXTRACT(MONTH FROM (sal.to_date)) AS to_date_month,
e.hire_date,
e.leave_date,
EXTRACT(YEAR FROM (e.leave_date)) AS leave_date_year
FROM salaries_hire_year sal,
employees e
WHERE sal.emp_no = e.emp_no
ORDER BY sal.emp_no ASC
;
-- e.hire_date AS emp_hire_date,
-- e.leave_date AS emp_leave_date,
